'''if-else'''

# skriv ut hvor mye penger det det er på konto dersom jeg har mer enn 9000,
#dersom det er mindre skriv ut 'Det er mindre enn det jeg får fra Lånekassen!'
konto = 9000

if (konto > 9000):
    print(f'Du har {konto} kroner på konto.')
else:
    print('Det er mindre enn det jeg får fra Lånekassen!')

