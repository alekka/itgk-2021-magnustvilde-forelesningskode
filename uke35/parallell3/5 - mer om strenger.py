'''Mer om strenger'''

# Hvordan kan vi få tak i en bokstav?
tekst = 'Dette er en streng!'
print(tekst[0])
print(tekst[1])
print(tekst[-1])
forste = tekst[0]
siste = tekst[-1]

# legge sammen strenger, gange strenger
leggeSammen = forste+siste
print(leggeSammen)
gangeSammen = leggeSammen*3
print(gangeSammen)


# legge sammen strenger og andre datatyper
sant = True

resultat = 'Min streng' + str(sant)
resultat2 = f'Min streng{sant}'
print(resultat)
print(resultat2)


tall = 4
flyt = 6.454
usant = False
treSammen = f'{tall}{flyt}{usant}'
print(treSammen)
