'''Escape-tegn'''

# Hvordan fungerer escape-tegn? Bruk \ sammen med ", ', n og t
'''
print('\"Hei!\"')
print('\'Hei!\'')
print('\"Denne linjen!\"\n Ny linje')
print('Her blir det \t \t avstand!')
'''

# skriv ut disse resultatene med god avstand på en linje:
vinn = 5
uavgjort = 10
tap = 4

print(f'{vinn}\t{uavgjort}\t{tap}')