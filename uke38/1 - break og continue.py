'''Break og continue'''

# skriv en for-løkke som går over tallene 1 til 10, og printer ut
# alle tallene utenom 6. Bruk continue på dette tallet. Skriv
# til skjermen når det hoppes over.
for i in range(1,11):
    if i == 6:
        print('Hopper over')
        continue
    print(i)


total = 0
# lag en for-løkke som går fra 10 til 20 som legger til ett
# og ett av disse tallene frem til totalverdien er over 50.
# Bryt ut av løkken med break og print ut verdien
for j in range(10,21):
    if total > 50:
        break
    total += j
print(total)

