'''
Oppsummering uke 39
'''
# returverdi
def min_funk():
    return 'Min returverdi'

# sette returverdi til resultat av funksjonskall
variabel = min_funk()
# importere moduler på forskjellige måter
#importere en hel modul, kjør gjennom å kalle på modulen
import math
ekte_pi = math.pi

#importere en spesifikk funksjon
from random import randint
# tilfeldig heltall mellom 0 og 10 (inneholder ytterpunktene)
randint(0,10)

#importere alle funksjoner i en modul, kjør direkte
from random import *
random()

#importere selvlagde moduler (lagret i samme mappe)
import min_modul
min_modul.run_fibo()