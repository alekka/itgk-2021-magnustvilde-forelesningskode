'''funksjon med returverdi og random-modulen'''

#Importer random-modulen (du trenger kun randint)
from random import randint

#Ta inn navnet til en bruker i en funksjon. Returner navnet
def navn():
    ditt_navn = input('Spiller sitt navn: ')
    return ditt_navn

#Bruk funksjonen til å lage to brukere
spiller1 = navn()
spiller2 = navn()

#Lag en funksjon som heter trill_terninger
    #Print ut info om spillet, samt hvordan man avslutter
    #De to brukerene skal trille terninger om og om igjen så lenge brukeren vil
    #Hold styr på hvor mange poeng de har
    #Gi ett poeng til den som får høyest terningkast, null dersom ulik
    #Returner hvor mange poeng spillerene har etter funksjonen er ferdig.
def trill_terninger():
    print('Velkommen til terningsspill!')
    print('Avslutt med N.')
    trille = input('Enter for å trille, N for å avslutte: ')
    poeng1, poeng2 = 0, 0
    while trille.lower() != 'n':
        trill1, trill2 = randint(1,6), randint(1,6) 
        print(f'{spiller1}: {trill1}')
        print(f'{spiller2}: {trill2}')
        # Gi poeng
        if trill1 > trill2:
            poeng1 += 1
            print(f'Poeng til {spiller1}.')
        elif trill2 > trill1:
            poeng2 += 1
            print(f'Poeng til {spiller2}')
        else:
            print('Ingen poeng.')
        trille = input('Enter for å trille, N for å avslutte: ')
    return poeng1, poeng2
trill_terninger()

# Lag en funksjon som tar inn to parametre.
# Funksjonen skal skrive ut poengscore og hvem som vinner.
