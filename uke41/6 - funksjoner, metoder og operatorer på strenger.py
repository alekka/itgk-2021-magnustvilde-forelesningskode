'''funksjoner metoder og operatorer på strenger'''

# lag en streng, og finn lengden av strengen med funksjonen len()
streng = 'Dette er min streng'
print(len(streng))

# lag en ny liste av strengen med funksjonen list()
min_liste = list(streng)
print(min_liste)

# bytt ut 'e' med 'a' i en streng med metoden replace
streng = streng.replace('e','a')
print(streng)

# del opp strengen på mellomrom med split()-metoden
heia = streng.split(' ')
print(heia)

