informasjon = ['Navn', 'Alder', 'Bosted']
personer = [['Kai', 45, 'Larvik'],
            ['John', 73, 'Røros'],
            ['Helle', 26, 'Oslo'],
            ['Karoline', 54, 'Bergen']]

for person in personer:
    for i in range(len(person)):
        print(f'{informasjon[i]}: {person[i]}')
    print()