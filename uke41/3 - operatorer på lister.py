'''operatorer på lister'''

navn = ['Ole', 'Dole', 'Doffen', 'Donald', 'Dolly', 'Skrue']
# bruk 'in'-operatoren til å finne ut om en verdi finnes i 'navn'
print('Mandag' in navn)
if 'Donald' in navn:
    print(navn)

# bruk del-operatoren til å slette en verdi i listen
del navn[2]
print(navn)
