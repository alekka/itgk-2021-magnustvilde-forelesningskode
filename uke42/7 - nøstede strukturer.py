'''Nøstede strukturer'''

# strukturen er {ticker: [pris_per_aksje, aksje, volum]}
aksjekurs_18okt = {'EQNR': [233.90, 'equinor', 3700000], 'DNB': [210.50, 'dnb bank asa', 382664],
                   'NHY': [71.04, 'norsk hydro', 3300000], 'PMG': [18.52, 'play magnus', 26063]}

# hent ut prisen for en aksje i PMG
print(aksjekurs_18okt['PMG'][0])

# hent ut det fulle navnet til PMG
print(aksjekurs_18okt['PMG'][1])

# lag en funksjon som returnerer en liste med alle navnene til selskapene (ikke ticker)
def navneliste():
    min_liste = []
    for ticker in aksjekurs_18okt:
#         print(ticker)
#         print(aksjekurs_18okt[ticker])
        min_liste.append(aksjekurs_18okt[ticker][1])
    return min_liste

print(navneliste())

# skriv ut alle fulle navn, men slik at første bokstav er stor
aksjeliste = navneliste()
for aksje in aksjeliste:
    print(aksje[0].upper() + aksje[1:])
