'''Dictionaries (ordbøker)'''

# lag en dictionary av lengde 2
magnus = {'alder': 26, 'bosted': 'Trondheim'}
print(len(magnus))

# legg til en nøkkel med en verdi og skriv ut dictionaryen
magnus['jobb'] = 'foreleser'
print(magnus)
