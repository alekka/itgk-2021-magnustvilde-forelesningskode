'''ekstra - bruke datastrukturer i sammenheng'''

# {ticker: aksjenavn, ...}
aksje_navn = {'EQNR':'equinor', 'DNB': 'dnb bank asa',
                   'NHY': 'norsk hydro', 'PMG': 'play magnus'}
# [[ticker, pris], ...]
aksje_verdi = [['EQNR', 233.90], ['DNB', 210.50], ['NHY', 71.04], ['PMG', 18.52]]


# lag en funksjon som henter verdier som hører til en ticker fra både
# aksje_navn og aksje_verdi


# lag en funksjon som henter ut verdier fra begge disse strukturene etter brukerens ønske


# lag en funksjon som henter ut verdier tilknyttet alle tickere
#(hint: bruk tidligere funksjon)


