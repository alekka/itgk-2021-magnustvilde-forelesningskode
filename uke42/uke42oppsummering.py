'''
oppsummering uke 42
'''

# mengder
a = set([1,2,3])
b = set([2,3,4])

# mengdeoperasjoner, forskjellige typer
a.union(b)

# dictionaries
person = {'name': 'Alex', 'age': 17, 'drivers license': False}

# aksessering av verdier via nøkler
person['name'] # gir 'Alex'
person['age'] # gir 17

# fjerne nøkkel + verdi
person.pop('drivers license')

# iterere over dictionaries
for key in person:
    print(key)
    print(person[key])

# nøstede strukturer
aksjekurs_18okt = {'EQNR': [233.90, 'equinor', 3700000], 'DNB': [210.50, 'dnb bank asa', 382664],
                   'NHY': [71.04, 'norsk hydro', 3300000], 'PMG': [18.52, 'play magnus', 26063]}

# iterere over og legge alle aksjenavn inn i en liste
min_liste = []
for ticker in aksjekurs_18okt:
    min_liste.append(aksjekurs_18okt[ticker][1])
