'''skrive til tekstfil'''

# åpne en tekstfil '2_tekst.txt'
with open('2_tekst.txt', 'w') as fil:
    # lag en tekststreng
    streng = 'Dette er en streng'
    # skriv strengen til filen
    fil.write(streng)
    # husk å lukke filen
