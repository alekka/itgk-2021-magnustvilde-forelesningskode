'''fra tekstfil til struktur i kode'''

# skriv innholdet i '5_aksjer.txt' til en dictionary
# på formen {ticker: [pris_aksje, navn_aksje]}
# pris_aksje skal være float
aksjer = {}
# Åpne fil med mulighet for å lese
with open('5_aksjer.txt', 'r') as fil:
    # Les innholdet inn i en liste
    linjer = fil.readlines()
    print(linjer)
    # Iterere over listen
    for linje in linjer:
        print(linje)
        # Splitt ved komma, ta bort ‘\n’
        innhold = linje.split(', ')
        print(innhold)
        # Gjør om til riktig datatype
        innhold[2] = innhold[2].strip('\n')
        innhold[1] = float(innhold[1])
        print(innhold)
        # Legg til elementene på riktig måte i dictionaryen
        nokkel, pris, aksjenavn = innhold[0], innhold[1], innhold[2]
        aksjer[nokkel] = [pris, aksjenavn]
        print(aksjer)
# Lukke filen
print(aksjer)