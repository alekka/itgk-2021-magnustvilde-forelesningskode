'''unntaksbehandling'''

# Lag til noe du VET gir feilmelding
# Finn feilmeldingen og håndter den med riktig type Exception.
# Skriv en beskjed til brukeren dersom det blir den typen exception.
try:
    hei
    print('Printes denne?')
except NameError:
    print('Det skjedde en feil!')

# prøv å legge sammen to forskjellige datatyper
# håndter feilen og lagre det i en variabel  som du skriver ut
try:
    legg_sammen = 'hei' + 5
except TypeError as error:
    print('Feilmelding:', error)

# ta inn input fra bruker som du gjør om til en float og
# bruk unntaksbehandling dersom input er på feil format.
try:
    svar = float(input('Tall: '))
except ValueError:
    print('Feil format.')
# legg til else og finally i strukturen
else:
    print('Riktig format.')
finally:
    print('Programmet er ferdig.')