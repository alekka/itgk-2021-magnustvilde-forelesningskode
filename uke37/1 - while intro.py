'''while-løkke intro'''

# print ut tallene fra 0 til og med 7
#(bruk en variabel for å holde styr på om løkken skal forsette å kjøre)
tall = 0
while tall<8:
    print(tall)
    tall += 1


# print ut tallene fra 10 til og med 20
#(bruk en boolsk variabel for å bestemme om løkken skal forsette å kjøre)
numb = 10
variabel = True
while variabel:
    print(numb)
    numb += 1
    if numb == 21:
        variabel = False

