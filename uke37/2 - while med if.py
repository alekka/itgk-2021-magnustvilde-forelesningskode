'''while og if'''

# Spør om input fra bruker frem til bruker skriver 'Exit'. Skriv ut det brukeren skrev hver gang

svar = input('Skriv \'Exit\' for å avslutte: ')
print(f'Du skrev: {svar}')

while svar != 'Exit':
    svar = input('Skriv \'Exit\' for å avslutte: ')
    print(f'Du skrev: {svar}')