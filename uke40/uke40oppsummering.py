'''
Oppsummering uke 40
'''

# liste
liste = [1,2,3]

# tuppel
tuppel = (1,2,3)

# indeks
liste[1]

# overskrive element i liste
liste[1] = 5

# legge til på slutten av liste
liste.append(7)

# legge til på indeks, flytte andre bakover
liste.insert(2, 'Indeks 2')

# fjerne siste element
liste.pop()

# fjerne element på indeks
del liste[0]

# iterere over liste via indeks
for i in range(len(liste)):
    print(liste[i])

# iterere over liste via elementer
for element in liste:
    print(element)

# + og * på lister
liste += ['Ny', 'liste', 'som', 'legges', 'til', 'på', 'slutten']
liste *= 2

# slicing av lister
liste[2:4]
liste[:3]
liste[3:]
liste[::2]
liste[::-1]