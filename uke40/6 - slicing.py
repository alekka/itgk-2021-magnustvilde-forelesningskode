'''Slicing'''

# lag en liste med tall fra 1 til 10:
liste = [1,2,3,4,5,6,7,8,9,10]
#   -   lag en ny liste som slicer ut tallene 5-10
ny_liste = liste[4:10]
print(ny_liste)

#   -   endre tallene 6-8 til 'Endret'
liste[5:8] = ['Endret']
print(liste)

#   -   legg til ['Hei', 'Hade'] etter indeks 3
liste[4:4] = ['Hei', 'Hade']
print(liste)

#   -   fjern elementene på indeks 2 og 3
liste[2:4] = []
print(liste)

#   -   print ut annenhvert element i listen
print(liste[::2])
