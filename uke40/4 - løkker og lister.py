'''Løkke på lister'''

# Iterer over en liste direkte, og skriv ut hvert element
liste = ['Hei', 'på', 'deg', '!']
for word in liste:
    print(word)

# Iterer over en liste ved hjelp av indeks, og endre annenhvert element
for i in range(len(liste)):
    if i % 2 == 0:
        liste[i] = 'Endret'
print(liste)

# Opprett en liste med fem tall. Iterer over listen og legg sammen tallene.
# Skriv ut resultatet.
tallene = [5,2,8,4,2]
total = 0
for tall in tallene:
    total += tall
print(total)

# Iterer over en liste og bruk både indeks og direkte aksess vha. enumerate()
# Skriv ut indeks og element
print()
for i, element in enumerate(tallene):
    print(i)
    print(element)


