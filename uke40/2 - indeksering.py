'''indeksering av lister og tupler'''

# opprett en liste med 5 elementer
liste = [1,2,3,4,5]

# hent ut elementer på tre plasser som du printer ut
forste = liste[4]
andre = liste[2]
tredje = liste[0]
print(f'Indeks 4: {forste}')
print(f'Indeks 2: {andre}')
print(f'Indeks 0: {tredje}')

print(liste[-1])
print(liste[len(liste)-1])
