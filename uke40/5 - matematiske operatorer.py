'''bruke matematiske operatorer på lister'''

første_liste = ['Hei', 'Hallo', 'Morna']
andre_liste = [1, 2, 3, 4]

# legg sammen de to listene i en ny liste
ny_liste = første_liste + andre_liste
print(ny_liste)

# dupliser verdiene i den nye listen
ny_liste *= 2
print(ny_liste)

# legg til en liste med to elementer på slutten av den første listen
første_liste += ['Hade', 'Hadebra']
print(første_liste)
