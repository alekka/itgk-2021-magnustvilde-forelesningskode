'''Middagsinspo'''

kjøtt = ['kjøttdeig', 'kylling', 'fisk']
karbo = ['pasta', 'ris', 'potet']
fyll = ['bønner', 'kikerter', 'linser']
grønt = ['løk', 'paprika', 'salat', 'brokkoli']

dager = ('Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag', 'Søndag')

# lag noen lister med dine foretrukne ingredienser,
# og skriv ut en tilfeldig blanding av de for hver dag
from random import choice
from random import randint

for dag in dager:
    print(f'{dag}: {choice(kjøtt)}, {choice(karbo)}, {choice(fyll)}, {choice(grønt)}')